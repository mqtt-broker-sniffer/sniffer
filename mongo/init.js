const { MongoClient } = require("mongodb");
const config = require("config");

/**
 * Initiate the mondoDB database
 */
init = async () => {
  // Connect to database
  const client = new MongoClient(
    `${config.get("dbURL")}/${config.get("dbName")}?directConnection=true`
  );
  const mongo = await client.connect();
  const db = client.db(config.get("dbName"));
  console.log("Connected");
  // Create collections
  const published = await db.createCollection("published");
  const counter = await db.createCollection("counters");
  const clients = await db.createCollection("clients");
  const topics = await db.createCollection("topics");
  console.log("Collection created");
  // Create index
  await published.createIndex({ brokerID: "hashed", sn: 1 });
  await clients.createIndex({ brokerID: "hashed" });
  await topics.createIndex({ brokerID: "hashed" });
  console.log("Index created");

  await mongo.close();

  return;
};

init();
