# MQTT Broker Recorder

The aim of this MQTT broker is to record the MQTT traffic going through it and to display this traffic thanks to the <a href="https://gitlab.com/mqtt-broker-sniffer/client">MQTT Viewer</a>.

# Deployment

## Manual installation

The first requirement is to install the latest version of MongoDB and NodeJS.

MongoDB has to be launched in replica set. The MongoDB service has to be
stopped before if it is already started. The simplest way is to deploy a replica set with
only one node and let it run in background.

```
mongod --bind_ip localhost --replSet rs0
```

Then, initiate the replica set in only one node.

```
mongo
rs.initiate()
quit()
```

You can then clone the gitlab repository of the broker, install the npm dependencies,
creates the collections and indexes, and start the broker.

```
git clone https://gitlab.com/mqtt-broker-sniffer/sniffer.git
cd ./sniffer
npm i
npm run init
npm run dock
```

This will start the broker with the ports specified in the configuration file.

## Docker

Copy the following instructions and place it in a `docker-compose.yml` file.

```yml
version: "3.3"

services:
  mqtt-broker:
    image: martindetienne/mqtt-broker-recorder:latest
    container_name: mqtt-broker
    ports:
      - "4040:4040"
      - "1883:1883"
    links:
      - mqtt-db
    depends_on:
      - mqtt-db

  mqtt-db:
    hostname: mqtt-db
    container_name: mqtt-db
    image: mongo:latest
    command: "--quiet --bind_ip_all --replSet rs0 "
    volumes:
      - mongo_db:/data/db

volumes:
  mongo_db:
```

Then, run the following command:

```
sudo docker -compose up -d
```

You have then to initiate the replica set how this is explained in the following section.

### Build in Docker

This is possible to build the broker in the docker environment. First, clone the gitlab repository, and build in docker compose.

```
git clone https://gitlab.com/mqtt-broker-sniffer/sniffer.git
cd ./sniffer
sudo docker -compose up -d
```

Once it’s done, initiate the MongoDB replica set and create the indexes with the following commands:

```
sudo docker exec -i -t mqtt-db /bin/bash
mongo
rs.initiate()
quit()
exit
sudo docker exec -i -t mqtt-broker /bin/bash
npm run init
exit
```

## Configuration

Several options are configurable in the broker. The configuration file location is `config/production`. If the one of the parameter is not specified, it will be replaced by its default value in `config/default.json`.

| Parameter  | Default                       | Description                                                                                                                                                                                                                                                                                |
| ---------- | ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| cors       | `{"origin": "*"}`             | The CORS policy for the API.                                                                                                                                                                                                                                                               |
| clientPort | `4040`                        | The port at which the viewer will connect to access the API.                                                                                                                                                                                                                               |
| mqttPort   | `1883`                        | The port for the MQTT clients.                                                                                                                                                                                                                                                             |
| dbURL      | `"mongodb://localhost:27017"` | The URL of the MongoDB database to store the data.                                                                                                                                                                                                                                         |
| dbName     | `"mqtt_proxy"`                | The name of the MongoDB database to store the data.                                                                                                                                                                                                                                        |
| bucket     | `None`                        | The specification of the token bucket for one MQTT broker. Type: `{"maxToken": number, "rate": number}`. `maxToken` is the maximum number of tokens in the bucket. `rate` is the number of tokens pushed into the bucket per minute. Does not apply the token bucket algorithm by default. |
| recordMax  | `None`                        | The maximum number of messages kept in the database for one MQTT broker. Type: `number`. Does not delete the messages by default.                                                                                                                                                          |

# Documentation

- You can find the documentation of the API <a href="./doc/API.md">here</a>.
- You can find the documentation of proxy between the MQTT broker, the API, and the database <a href="./doc/records.md">here</a>.
