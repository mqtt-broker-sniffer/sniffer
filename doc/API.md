# GET /published/:id

Get the list of published messages on a given broker.

## Parameters

- `id`: The ID of the broker from which the messages are fetched.

## Query

- `last`: The number of plublished messages that should be sent. The messages are always de last. The query `from` is ignored if the query `last` is set.
- `from`: The returned messages should have a serial number above or equal to the value specified.

## Response

- `200`:

  ```JSON
  [
      {
          "topic": "string" ,
          "sender": "string",
          "message": "string",
          "sn": "number",
          "qos": 0|1|2,
          "receivers": ["string"],
          "time": "number",
      }
  ]
  ```

  The array of messages returned in the order of the serial number.

- `404`: `string` when the specified ID does not exist

# GET /new_broker

Create a new MQTT broker and return its ID.

## Response

- `200`: `string` the ID of the new broker

# GET /clients/:id

Get the list of clients who have already been connected to the MQTT broker (or a still connected).

## Parameters

- `id`: The ID of the broker from which the clients has been connected

## Response

- `200`:

  ```JSON
  [
      {
          "connected": "boolean",
          "client": "string",
          "ip": "string",
      }
  ]
  ```

  The array of clients who have already been connected to this MQTT broker.

- `404`: `string` when the specified ID does not exist

# GET /topics/:id

Get the list of topics which have already been used on this MQTT broker.

## Parameters

- `id`: The ID of the broker from which the topics has been used

## Response

- `200`:

  ```JSON
  [
      {
          "topic": "string",
          "subscribed": ["string"],
      }
  ]
  ```

  The array of topics who have already been used in this MQTT broker.

- `404`: `string` when the specified ID does not exist
