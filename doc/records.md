# Records

`records.js` makes the connection between the <a href="https://www.fastify.io/">fastify</a> API, the <a href="https://github.com/moscajs/aedes">Aedes</a> MQTT Broker and the database.

## Usage

```JavaScript
const records = require("src/records")
```

# Methods

## init()

Create a MQTT Recorder

<br/>

## fetchPublication(brokerID, from)

Fetch the recorded published messages on a broker from a given serial number

### Parameters

| Field    | Type     | Description                                                  |
| -------- | -------- | ------------------------------------------------------------ |
| brokerID | `string` | the ID (username) of the broker                              |
| from     | `number` | the serial number from which the recent message are returned |

### Throws

`string`: if the broker id or the serial number does not exists

### Returns

`[ { topic: string, message: Buffer, qos: number, sn: number, sender: string, time: number } ]`: the recent published messages

<br/>

## createBroker()

Create a new MQTT broker and give a username

### Returns

`string`: a username to use with this broker

<br/>

## recordPublication(client, packet)

Add a publication from a sender, modify the payload to add the serial number in it

### Parameters

| Field  | Type                                                                           | Description                          |
| ------ | ------------------------------------------------------------------------------ | ------------------------------------ |
| client | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client which publish the message |
| packet | <a href="https://github.com/mqttjs/mqtt-packet#publish">PublishPacket</a>      | the packet of the message            |

### Throws

`string`: if the id (username) is not valid

<br/>

## recordSubscribe(client, topic)

Record when a client subscribe to a topic

### Parameters

| Field  | Type                                                                           | Description                            |
| ------ | ------------------------------------------------------------------------------ | -------------------------------------- |
| client | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client who subscribed to the topic |
| topic  | `string`                                                                       | the subscribed topic                   |

<br/>

## recordUnsubscribe(client, topic)

Record when a client unsubscribe from a topic

### Parameters

| Field  | Type                                                                           | Description                              |
| ------ | ------------------------------------------------------------------------------ | ---------------------------------------- |
| client | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client who unsubscribed to the topic |
| topic  | `string`                                                                       | the unsubscribed topic                   |

<br/>

## getTopics(brokerID)

Fetch all the topics of a broker

### Parameters

| Field    | Type     | Description       |
| -------- | -------- | ----------------- |
| brokerID | `string` | the ID the broker |

### Returns

`[{topic: string, subscribed: [string], brokerID: string}]`: the list of topics on this broker

<br/>

## fetchLastPublication(brokerID, last)

Fetch the most recent recorded published messages on a broker

### Parameters

| Field    | Type     | Description                                                                                                      |
| -------- | -------- | ---------------------------------------------------------------------------------------------------------------- |
| brokerID | `string` | the ID (username) of the broker                                                                                  |
| last     | `number` | the number of last publications that should be returned, the number of returned messages could be slightly above |

### Throws

`string`: if the broker id or the serial number does not exists

### Returns

`[ { topic: string, message: Buffer, qos: number, sn: number, sender: string, time: number } ]`: the recent published messages

<br/>

## addReceiver(brokerID, sn, client)

Add a receiver of the message

### Parameters

| Field    | Type                                                                           | Description                                                  |
| -------- | ------------------------------------------------------------------------------ | ------------------------------------------------------------ |
| brokerID | `string`                                                                       | the ID of the MQTT broker in which this message is published |
| sn       | `number`                                                                       | the serial number of the message sent                        |
| client   | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client who receive this message                          |

### Throws

`string`: if the broker id or the serial number does not exists

<br/>

## isValidID(brokerID)

Verify if the ID of a broker (username) is valid

### Parameters

| Field    | Type     | Description                            |
| -------- | -------- | -------------------------------------- |
| brokerID | `string` | the ID of the broker we want to verify |

### Returns

`boolean`: true if the ID is valid

<br/>

## addClient(client, brokerID)

Add a client

### Parameters

| Field    | Type                                                                           | Description          |
| -------- | ------------------------------------------------------------------------------ | -------------------- |
| brokerID | `string`                                                                       | the id of the broker |
| client   | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client to add    |

<br/>

## disconnectClient(client, brokerID)

Disconnect a client on a given broker

### Parameters

| Field    | Type                                                                           | Description              |
| -------- | ------------------------------------------------------------------------------ | ------------------------ |
| brokerID | `string`                                                                       | the ID of the broker     |
| client   | <a href="https://github.com/moscajs/aedes/blob/main/docs/Client.md">Client</a> | the client to disconnect |

<br/>

## getClients(brokerID)

Fetch all the clients

### Parameters

| Field    | Type     | Description          |
| -------- | -------- | -------------------- |
| brokerID | `string` | the ID of the broker |

### Returns

`[{connected: boolean, client: string, brokerID: string}]`: the list of client on this broker
