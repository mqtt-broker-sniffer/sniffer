const records = require("../records");

/**
 * A route to get the pusblished messages
 *
 * @param {import("fastify").FastifyInstance} fastify the fastify instance
 */
module.exports = async function (fastify, _) {
  fastify.route({
    method: "GET",
    url: "/published/:id",
    schema: {
      querystring: {
        from: { type: "number" },
        last: { type: "number" },
      },
      response: {
        200: {
          type: "array",
          items: {
            type: "object",
            properties: {
              topic: { type: "string" },
              sender: { type: "string" },
              message: { type: "string" },
              sn: { type: "number" },
              qos: { type: "number", maximum: 2, minimum: 0 },
              receivers: { type: "array", items: { type: "string" } },
              time: { type: "number" },
            },
          },
        },
        404: {
          type: "string",
        },
      },
    },
    handler: async (request, reply) => {
      if (!(await records.isValidID(request.params.id))) {
        reply.code(404);
        return "Not found";
      } else {
        if (request.query.last !== undefined) {
          return await records.fetchLastPublication(
            request.params.id,
            request.query.last
          );
        }
        return records.fetchPublication(
          request.params.id,
          request.query.from === undefined ? 0 : request.query.from
        );
      }
    },
  });
};
