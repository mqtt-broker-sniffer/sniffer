const records = require("../records");

/**
 * A route to fetch the client
 *
 * @param {import("fastify").FastifyInstance} fastify the fastify instance
 */
module.exports = async function (fastify, _) {
  fastify.route({
    method: "GET",
    url: "/clients/:id",
    schema: {
      response: {
        200: {
          type: "array",
          items: {
            type: "object",
            properties: {
              connected: { type: "boolean" },
              client: { type: "string" },
              ip: { type: "string" },
            },
          },
        },
        404: {
          type: "string",
        },
      },
    },
    handler: async (request, reply) => {
      if (!(await records.isValidID(request.params.id))) {
        reply.code(404);
        return "Not found";
      } else {
        return await records.getClients(request.params.id);
      }
    },
  });
};
