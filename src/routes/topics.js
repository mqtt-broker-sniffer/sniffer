const records = require("../records");

/**
 * A route to get topics of a broker
 *
 * @param {import("fastify").FastifyInstance} fastify the fastify instance
 */
module.exports = async function (fastify, _) {
  fastify.route({
    method: "GET",
    url: "/topics/:id",
    schema: {
      response: {
        200: {
          type: "array",
          items: {
            type: "object",
            properties: {
              topic: { type: "string" },
              subscribed: {
                type: "array",
                items: { type: "string" },
              },
            },
          },
        },
        404: {
          type: "string",
        },
      },
    },
    handler: async (request, reply) => {
      if (!(await records.isValidID(request.params.id))) {
        reply.code(404);
        return "Not found";
      } else {
        return await records.getTopics(request.params.id);
      }
    },
  });
};
