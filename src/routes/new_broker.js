const records = require("../records");

/**
 * The route to create a new broker
 *
 * @param {import("fastify").FastifyInstance} fastify the fastify instance
 */
module.exports = async function (fastify, _) {
  fastify.route({
    method: "GET",
    url: "/new_broker",
    schema: {
      response: {
        200: {
          type: "string",
        },
      },
    },
    handler: async (request, reply) => {
      return records.createBroker();
    },
  });
};
