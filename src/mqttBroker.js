const config = require("config");
const aedes = require("aedes").Server();
const { createServer } = require("aedes-server-factory");
const { AuthErrorCode } = require("aedes");

const port = config.get("mqttPort");

/**
 * Launch a modified MQTT broker to record the traffic
 *
 * @param {import("./records")} records the records of the MQTT brokers
 */
module.exports = (records) => {
  const server = createServer(aedes);

  // Start the MQTT broker
  server.listen(port, function () {
    console.log("MQTT Broker started on port", port);
  });

  /**
   * The handlers are used to firstly record the packets of the topics but also to
   * allow to separate different users using the allocated username.
   *
   * The id of the clients and the topics used by the user are modified to have
   * <username>/ concatenated at the front but this modification is hidden to the users
   */

  // Modify the subscription to topics
  aedes.authorizeSubscribe = function (client, sub, callback) {
    records.recordSubscribe(client, sub.topic);
    callback(null, {
      ...sub,
      topic: client.id.slice(0, client.id.indexOf("/") + 1) + sub.topic,
    });
  };

  // Modify the subscription to the modified topics and record the unsubscribtion
  aedes.addListener("unsubscribe", (subscriptions, client) => {
    const brokerID = client.id.slice(0, client.id.indexOf("/"));
    subscriptions.forEach((topic) => {
      if (!topic.startsWith(brokerID)) {
        records.recordUnsubscribe(client, topic);
        client.unsubscribe(brokerID + "/" + topic, (error) => {});
      }
    });
  });

  // Modify the authentification to modify the client ID
  aedes.authenticate = async function (client, username, password, callback) {
    // Verify the ID of the broker
    if (!(await records.isValidID(username))) {
      const error = new Error("Auth error");
      error.returnCode = 4;
      callback(error, null);
      return;
    }
    // Verify if a client with the same ID is not connected
    const sameId = (await records.getClients(username)).find(
      (client_) => client_.client === client.id
    );
    if (sameId !== undefined && sameId.connected) {
      const error = new Error("Bad ID");
      error.returnCode = 2;
      callback(error, null);
      return;
    }

    client.id = username + "/" + client.id;
    await records.addClient(client, username);
    callback(null, true);
  };

  // Modify the disconnection to record that a client is not connected anymore
  aedes.addListener("clientDisconnect", async (client) => {
    const brokerID = client.id.slice(0, client.id.indexOf("/"));
    await records.disconnectClient(client, brokerID);
  });

  // Modify the publishment of messages on topics
  aedes.authorizePublish = async function (client, packet, callback) {
    const brokerID = client.id.slice(0, client.id.indexOf("/"));
    packet.topic = brokerID + "/" + packet.topic;
    try {
      await records.recordPublication(client, packet);
      callback(null);
    } catch (e) {
      callback(new Error(e));
    }
  };

  // Modify the forwarding of messages to subscribed clients
  aedes.authorizeForward = function (client, packet) {
    const brokerID = client.id.slice(0, client.id.indexOf("/"));
    records.addReceiver(brokerID, packet.payload.sn, client);
    return {
      ...packet,
      payload: packet.payload.message,
      topic: packet.topic.slice(packet.topic.search("/") + 1),
    };
  };
};
