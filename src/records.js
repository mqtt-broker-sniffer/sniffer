const config = require("config");
const { Db, ObjectId } = require("mongodb");
const { MongoClient } = require("mongodb");

const txsOptn = {
  readPreference: "primary",
  readConcern: { level: "local" },
  writeConcern: { w: "majority" },
};

/**
 * A set records of several MQTT brokers
 */
module.exports = {
  /**
   * The database of the recorded MQTT messages
   *
   * @type {Db}
   */
  db: undefined,

  /**
   * The client of the database
   *
   * @type {MongoClient}
   */
  client: undefined,

  /**
   * Create a MQTT Recorder
   */
  init: async () => {
    const client = new MongoClient(
      `${config.get("dbURL")}/${config.get("dbName")}?directConnection=true`
    );
    this.client = await client.connect();
    this.db = client.db(config.get("dbName"));
    console.log(
      `Connected to database ${config.get("dbURL")}/${config.get("dbName")}`
    );
    // Reset the connection status of all the old connected clients and set of subscribed clients
    const session = this.client.startSession();
    try {
      await session.withTransaction(async () => {
        await this.db
          .collection("clients")
          .updateMany(
            { connected: true },
            { $set: { connected: false } },
            { session }
          );
        await this.db
          .collection("topics")
          .updateMany({}, { $set: { subscribed: [] } }, { session });
      }, txsOptn);
    } finally {
      session.endSession();
    }

    /**
     * Fetch the recorded published messages on a broker from a given serial number
     *
     * @param {string} brokerID the ID (username) of the broker
     * @param {number} from the serial number from which the recent message are returned
     *
     * @returns {[
     *      {
     *        topic: string,
     *        message: Buffer,
     *        qos: number,
     *        sn: number,
     *        sender: string,
     *        time: number
     *      }
     *    ]} the recent published messages
     *
     * @throws {string} if the broker id or the serial number does not exists
     */
    this.fetchPublication = async (brokerID, from) => {
      const session = this.client.startSession();
      let result;

      try {
        await session.withTransaction(async () => {
          // Get the count of published message in this broker
          const count = (
            await this.db
              .collection("counters")
              .findOne({ _id: new ObjectId(brokerID) }, { session })
          ).count;
          // Verify if the broker ID exists
          if (count == null) {
            session.abortTransaction();
            throw "The id of the broker is not valid";
          } else if (count <= from) {
            // Empty array if the serial number does not exist
            result = [];
          } else {
            result = await this.db
              .collection("published")
              .find({ brokerID, sn: { $gte: from } }, { session })
              .sort({ sn: 1 })
              .toArray();
          }
        }, txsOptn);
      } finally {
        session.endSession();
        return result;
      }
    };
  },

  /**
   * Fetch the recorded published messages on a broker from a given serial number
   *
   * @param {string} brokerID the ID (username) of the broker
   * @param {number} from the serial number from which the recent message are returned
   *
   * @returns {[
   *      {
   *        topic: string,
   *        message: Buffer,
   *        qos: number,
   *        sn: number,
   *        sender: string,
   *        time: number
   *      }
   *    ]} the recent published messages
   *
   * @throws {string} if the broker id or the serial number does not exists
   */
  fetchPublication: async (brokerID, from) => {
    const session = this.client.startSession();
    let result;

    try {
      await session.withTransaction(async () => {
        // Get the count of published message in this broker
        const count = (
          await this.db
            .collection("counters")
            .findOne({ _id: new ObjectId(brokerID) }, { session })
        ).count;
        // Verify if the broker ID exists
        if (count == null) {
          session.abortTransaction();
          throw "The id of the broker is not valid";
        } else if (count <= from) {
          // Empty array if the serial number does not exist
          result = [];
        } else {
          result = await this.db
            .collection("published")
            .find({ brokerID, sn: { $gte: from } }, { session })
            .sort({ sn: 1 })
            .toArray();
        }
      }, txsOptn);
    } finally {
      session.endSession();
      return result;
    }
  },

  /**
   * Create a new MQTT broker and give a username
   *
   * @returns {string} a username to use with this broker
   */
  createBroker: async () => {
    const session = this.client.startSession();
    let result;
    try {
      await session.withTransaction(async () => {
        result = (
          await this.db.collection("counters").insertOne(
            {
              count: 0,
              bucket: config.has("bucket") ? config.get("bucket.maxToken") : 0,
            },
            { session }
          )
        ).insertedId.toString();
      }, txsOptn);
    } finally {
      session.endSession();
      return result;
    }
  },

  /**
   * Add a publication from a sender, modify the payload to add the serial number in it
   *
   * @param {import("aedes").Client} client the client which publish the message
   * @param {import("aedes").PublishPacket} packet the packet of the message
   *
   * @throws {string} if the id (username) is not valid
   */
  recordPublication: async (client, packet) => {
    const session = this.client.startSession();
    const brokerID = client.id.slice(0, client.id.indexOf("/"));
    const topic = packet.topic.slice(packet.topic.indexOf("/") + 1);

    try {
      await session.withTransaction(async () => {
        // Get the count of published message in this broker
        const count = await this.db
          .collection("counters")
          .findOne({ _id: new ObjectId(brokerID) }, { session });
        // Verify if the broker ID exists
        if (count === null) {
          session.abortTransaction();
          throw "The id of the broker is not valid";
        }

        // Only take into account the bucket if in configuration file
        if (config.has("bucket")) {
          // Fetch the last message
          const lastPublication = await this.db
            .collection("published")
            .findOne({ brokerID, sn: count.count - 1 }, { session });

          // Compute the state of the bucket if this packet was added
          let newBucket = count.bucket;
          if (lastPublication !== null) {
            // Compute the number of tokens that should be added since the last message
            const additionalToken =
              config.get("bucket.rate") *
              ((Date.now() - lastPublication.time) / 60000);
            if (newBucket + additionalToken > config.get("bucket.maxToken")) {
              // We do not exceed the bucket
              newBucket = config.get("bucket.maxToken") - 1;
            } else {
              newBucket += additionalToken - 1;
            }
          } else {
            newBucket = config.get("bucket.maxToken") - 1;
          }

          // Verify if the bucket is empty (the message is thrown if the bucket is empty)
          if (newBucket < 0) {
            throw "The load is too high";
          }

          // Update the bucket
          await this.db
            .collection("counters")
            .updateOne(
              { _id: new ObjectId(brokerID) },
              { $set: { bucket: newBucket } },
              { session }
            );
        }

        // Only take into account the removal of old messages if in configuration file
        if (config.has("recordMax")) {
          // Remove too old publication
          await this.db.collection("published").deleteMany(
            {
              brokerID,
              sn: { $lt: count.count - config.get("recordMax") },
            },
            { session }
          );
        }

        // Insert the message
        await this.db.collection("published").insertOne(
          {
            brokerID,
            topic,
            qos: packet.qos,
            message: packet.payload,
            sender: client.id.slice(client.id.indexOf("/") + 1),
            sn: count.count,
            receivers: [],
            time: Date.now(),
          },
          { session }
        );
        // Update the counter
        await this.db
          .collection("counters")
          .updateOne(
            { _id: new ObjectId(brokerID) },
            { $inc: { count: 1 } },
            { session }
          );
        // Add the topic if it does not exist yet
        if (
          (await this.db
            .collection("topics")
            .findOne({ brokerID, topic }, { session })) === null
        ) {
          await this.db.collection("topics").insertOne(
            {
              brokerID,
              topic,
              subscribed: [],
            },
            { session }
          );
        }
        // Change the payload to add the serial number
        packet.payload = {
          message: packet.payload,
          sn: count.count,
        };
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Record when a client subscribe to a topic
   *
   * @param {import("aedes").Client} client the client who subscribed to the topic
   * @param {string} topic the subscribed topic
   */
  recordSubscribe: async (client, topic) => {
    const session = this.client.startSession();
    const brokerID = client.id.slice(0, client.id.indexOf("/"));

    try {
      await session.withTransaction(async () => {
        // Add the topic does not exist yet, update otherwise
        if (
          (await this.db
            .collection("topics")
            .findOne({ brokerID, topic }, { session })) === null
        ) {
          await this.db.collection("topics").insertOne(
            {
              brokerID,
              topic,
              subscribed: [client.id.slice(client.id.indexOf("/") + 1)],
            },
            { session }
          );
        } else {
          await this.db.collection("topics").updateOne(
            { brokerID, topic },
            {
              $addToSet: {
                subscribed: client.id.slice(client.id.indexOf("/") + 1),
              },
            },
            { session }
          );
        }
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Record when a client unsubscribe from a topic
   *
   * @param {import("aedes").Client} client the client who unsubscribed from the topic
   * @param {string} topic the unsubscribed topic
   */
  recordUnsubscribe: async (client, topic) => {
    const session = this.client.startSession();
    const brokerID = client.id.slice(0, client.id.indexOf("/"));

    try {
      await session.withTransaction(async () => {
        // Remove the client from the subscribtion of this topic
        await this.db.collection("topics").updateOne(
          { brokerID, topic },
          {
            $pull: {
              subscribed: client.id.slice(client.id.indexOf("/") + 1),
            },
          },
          { session }
        );
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Fetch all the topics of a broker
   *
   * @param {string} brokerID the ID the broker
   *
   * @returns {[{topic: string, subscribed: [string], brokerID: string}]} the list of topics on this broker
   */
  getTopics: async (brokerID) => {
    const session = this.client.startSession();
    let result;

    try {
      await session.withTransaction(async () => {
        result = await this.db
          .collection("topics")
          .find({ brokerID }, { session })
          .toArray();
      }, txsOptn);
    } finally {
      session.endSession();
      return result;
    }
  },

  /**
   * Fetch the most recent recorded published messages on a broker
   *
   * @param {string} brokerID the ID (username) of the broker
   * @param {number} last the number of last publications that should be returned, the number of returned messages could be slightly above
   *
   * @returns {[
   *      {
   *        topic: string,
   *        message: Buffer,
   *        qos: number,
   *        sn: number,
   *        sender: string,
   *        time: number
   *      }
   *    ]} the recent published messages
   *
   * @throws {string} if the broker id or the serial number does not exists
   */
  fetchLastPublication: async (brokerID, last) => {
    // Get the current count
    const count = (
      await this.db
        .collection("counters")
        .findOne({ _id: new ObjectId(brokerID) })
    ).count;
    // Verify if the broker ID exists
    if (count == null) {
      throw "The id of the broker is not valid";
    } else {
      return this.fetchPublication(brokerID, Math.max(0, count - last));
    }
  },

  /**
   * Add a receiver of the message
   *
   * @param {string} brokerID the ID of the MQTT broker in which this message is published
   * @param {number} sn the serial number of the message sent
   * @param {import("aedes").Client} client the client who receive this message
   *
   * @throws {string} if the broker id or the serial number does not exists
   */
  addReceiver: async (brokerID, sn, client) => {
    const session = this.client.startSession();

    try {
      await session.withTransaction(async () => {
        if (
          (await this.db.collection("published").updateOne(
            { brokerID, sn },
            {
              $addToSet: {
                receivers: client.id.slice(client.id.indexOf("/") + 1),
              },
            },
            { session }
          )) == null
        ) {
          session.abortTransaction();
          throw "The serial number of the broker ID does not exist";
        }
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Verify if the ID of a broker (username) is valid
   *
   * @param {number} brokerID the ID of the broker we want to verify
   *
   * @returns {boolean} true if the ID is valid
   */
  isValidID: async (brokerID) => {
    const session = this.client.startSession();
    let result;

    try {
      await session.withTransaction(async () => {
        // Get the count of published message in this broker
        const count = (
          await this.db
            .collection("counters")
            .findOne({ _id: new ObjectId(brokerID) }, { session })
        ).count;
        result = count !== null;
      }, txsOptn);
    } finally {
      session.endSession();
      return result;
    }
  },

  /**
   * Add a client
   *
   * @param {import("aedes").Client} client the client to add
   * @param {string} brokerID the id of the broker
   */
  addClient: async (client, brokerID) => {
    const session = this.client.startSession();

    try {
      await session.withTransaction(async () => {
        // Insert a user only once
        if (
          (await this.db.collection("clients").findOne(
            {
              brokerID,
              client: client.id.slice(client.id.indexOf("/") + 1),
            },
            { session }
          )) === null
        ) {
          await this.db.collection("clients").insertOne(
            {
              brokerID,
              client: client.id.slice(client.id.indexOf("/") + 1),
              connected: true,
              ip: client.conn.remoteAddress,
            },
            { session }
          );
        } else {
          // If this id existed, put connect to true
          await this.db
            .collection("clients")
            .updateOne(
              { brokerID, client: client.id.slice(client.id.indexOf("/") + 1) },
              { $set: { connected: true, ip: client.conn.remoteAddress } },
              { session }
            );
        }
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Disconnect a client on a given broker
   *
   * @param {import("aedes").Client} client the client to disconnect
   * @param {string} brokerID the ID of the broker
   */
  disconnectClient: async (client, brokerID) => {
    const session = this.client.startSession();
    const clientID = client.id.slice(client.id.indexOf("/") + 1);

    try {
      await session.withTransaction(async () => {
        await this.db
          .collection("clients")
          .updateOne(
            { brokerID, client: clientID },
            { $set: { connected: false } },
            { session }
          );
        await this.db
          .collection("topics")
          .updateMany(
            { brokerID, subscribed: { $in: [clientID] } },
            { $pull: { subscribed: clientID } },
            { session }
          );
      }, txsOptn);
    } finally {
      session.endSession();
    }
  },

  /**
   * Fetch all the clients
   *
   * @param {string} brokerID the ID the broker
   *
   * @returns {[{connected: boolean, client: string, brokerID: string}]} the list of client on this broker
   */
  getClients: async (brokerID) => {
    const session = this.client.startSession();
    let result;

    try {
      await session.withTransaction(async () => {
        result = await this.db
          .collection("clients")
          .find({ brokerID }, { session })
          .toArray();
      }, txsOptn);
    } finally {
      session.endSession();
      return result;
    }
  },
};
