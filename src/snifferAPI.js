const config = require("config");
const fastify = require("fastify").default({ logger: true });

const port = config.get("clientPort");

/**
 * Launch the API to fetch the MQTT records
 */
module.exports = async () => {
  fastify.register(require("fastify-cors"), config.get("cors"));
  fastify.register(require("./routes/new_broker"));
  fastify.register(require("./routes/published"));
  fastify.register(require("./routes/clients"));
  fastify.register(require("./routes/topics"));

  try {
    await fastify.listen(port, "0.0.0.0");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
