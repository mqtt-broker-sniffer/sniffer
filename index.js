const startMqttBroker = require("./src/mqttBroker");
const records = require("./src/records");
const startSnifferAPI = require("./src/snifferAPI");

records.init();
startMqttBroker(records);
startSnifferAPI();
