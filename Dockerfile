FROM node:16.15.0
WORKDIR /broker
COPY . .
RUN npm install
CMD npm run dock
